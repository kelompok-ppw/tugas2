"""bukuwajah URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import profil.urls as profil 
import cari_mahasiswa.urls as cari_mahasiswa
import login_status.urls as login_status
import riwayat.urls as riwayat

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include(login_status, namespace='login-status')),
    url(r'^profil/', include(profil,namespace='profil')),

    url(r'^cari-mahasiswa/', include(cari_mahasiswa, namespace = 'cari-mahasiswa')),

    url(r'^riwayat/', include(riwayat,namespace='riwayat')),

]
