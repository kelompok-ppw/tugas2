from django.shortcuts import render
from login_status.models import User
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from profil.models import UserLinkedIn, KeahlianUser

# Create your views here.
response = {}
def index(request):
	print ("#==> index cari_mahasiswa")
	if 'user_login' in request.session:
		response['is_login'] = True
		return HttpResponseRedirect(reverse('cari-mahasiswa:mahasiswa-list'))
	else:
		response['is_login'] = False
		html = 'cari_mahasiswa/mahasiswa'
		return render(request, html, response)
	
def mahasiswa_list(request):
	mahasiswa_list = User.objects.all().values()
	return mahasiswa_list

def mahasiswa_listKeahlian(request):
	mahasiswa_listKeahlian = KeahlianUser.objects.all().values()
	return mahasiswa_listKeahlian