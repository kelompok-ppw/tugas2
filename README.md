## Pipeline Status
[![pipeline status](https://gitlab.com/kelompok-ppw/tugas2/badges/master/pipeline.svg)](https://gitlab.com/kelompok-ppw/tugas2/commits/master)

## Code Coverage Status
[![coverage report](https://gitlab.com/kelompok-ppw/tugas2/badges/master/coverage.svg)](https://gitlab.com/kelompok-ppw/tugas2/commits/master)

## Important Link
- Herokuapp Link : https://bukuwajah.herokuapp.com/
- Repo Link : https://gitlab.com/kelompok-ppw/tugas2

## Made With Love By
- Aldi Hilman Ramadhani (1606895184)
- Fadhlan Zakiri (1606874620)
- Muhammad Imam Santosa (1606894635)
- Zaki Raihan (1606878505)
