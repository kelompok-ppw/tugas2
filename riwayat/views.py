from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from .api_riwayat import get_riwayat
from profil.models import UserLinkedIn
from login_status.models import User

response = {'author' : 'Fadhlan Zakiri'}

def index(request):
	response['riwayat'] = get_riwayat()
	html = 'riwayat/riwayat.html'
	return render(request, html, response)

def hidden_or_not(request):
	username = request.session['user_login']
	kode_identitas = request.session['kode_identitas']
	usernya= User.objects.get(identity_number=kode_identitas, name=username)
	userLinkedIn, sukses= UserLinkedIn.objects.get_or_create(user=usernya)
	return userLinkedIn.user_tampilkan_nilai