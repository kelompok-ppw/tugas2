import requests

ACADEMIC_API = 'https://private-e52a5-ppw2017.apiary-mock.com/riwayat'

def get_riwayat():
    riwayat = requests.get(ACADEMIC_API)
    return riwayat.json()

