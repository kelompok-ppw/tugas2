# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.core.exceptions import ObjectDoesNotExist
from django.utils.datastructures import MultiValueDictKeyError
from .models import UserLinkedIn, KeahlianUser
from login_status.models import User
import json

response = {}

def index(request):
    print ("#==> masuk index")
    if 'user_login' in request.session:
        response['is_login'] = True
        return HttpResponseRedirect(reverse('profil:profile-edit'))
    else:
        response['is_login'] = False
        html = 'login/login.html'
        return render(request, html, response)

def profile_edit(request):
    if 'user_login' in request.session:
        response['is_login'] = True
        response['author'] = "Zaki Raihan"
        username = request.session['user_login']
        kode_identitas = request.session['kode_identitas']
        usernya= User.objects.get(identity_number=kode_identitas, name=username)
        userLinkedIn, is_ada_linkedin = UserLinkedIn.objects.get_or_create(user=usernya)
        response['user'] = usernya
        response['userLinkedIn'] = userLinkedIn
        response['keahlianUser'] = KeahlianUser.objects.filter(user_linkedin=userLinkedIn).all()
        response['is_ada_linkedin'] = is_ada_linkedin
    html = "profil/profil.html"
    return render(request, html, response)

@csrf_exempt
def update_user_profile(request):
    user_name = request.POST['user_name']
    user_email = request.POST['user_email']
    user_profile = request.POST['user_profile']
    try:
        user_image = request.POST['user_image']
    except MultiValueDictKeyError:
        user_image = None
    user_headline = request.POST['user_headline']
    username = request.session['user_login']
    kode_identitas = request.session['kode_identitas']
    
    usernya= User.objects.get(identity_number=kode_identitas, name=username)
    
    user_linkedin = UserLinkedIn.objects.filter(user=usernya)
    user_linkedin.user_name = user_name
    user_linkedin.user_email = user_email
    user_linkedin.user_image = user_image
    user_linkedin.user_headline = user_headline
    
    count = UserLinkedIn.objects.filter(user=usernya, user_name=user_name, user_email=user_email, user_profile=user_profile, user_image=user_image, user_headline=user_headline).count()
    UserLinkedIn.objects.filter(user=usernya).update(user_name=user_name, user_email=user_email, user_profile=user_profile, user_image=user_image, user_headline=user_headline)
    data = {
        'belum_login': (count == 0)  
    }
    return JsonResponse(data)

@csrf_exempt
def update_user_keahlian(request):
	user_tampilkan_nilai = request.POST['user_tampilkan_nilai']
	username = request.session['user_login']
	kode_identitas = request.session['kode_identitas']
	usernya= User.objects.get(identity_number=kode_identitas, name=username)
	user_linkedin, berhasil = UserLinkedIn.objects.get_or_create(user=usernya)
	list_kemampuan = json.loads(request.POST["list_dari_app"])
	KeahlianUser.objects.filter(user_linkedin=user_linkedin).delete()
	for i in range(0, len(list_kemampuan)):
		dictioKemampuan = list_kemampuan[str(i)]
		KeahlianUser.objects.get_or_create(user_linkedin=user_linkedin, user_index_bahasa=dictioKemampuan['index_bahasa'] ,
	 	user_bahasa=dictioKemampuan['bahasa'] , user_index_keahlian=dictioKemampuan['index_keahlian'] ,
		 user_tingkatan=dictioKemampuan['tingkatan'] , user_warna_keahlian=dictioKemampuan['warna_keahlian'] );

	print(KeahlianUser.objects.filter(user_linkedin=user_linkedin).count())

	UserLinkedIn.objects.filter(user=usernya).update(user_tampilkan_nilai=user_tampilkan_nilai)
	data = {
        'belum_login': True  
    }
	return JsonResponse(data)

@csrf_exempt
def get_keahlian_json(request):
    username = request.session['user_login']
    kode_identitas = request.session['kode_identitas']
    usernya= User.objects.get(identity_number=kode_identitas, name=username)
    userLinkedIn, berhasil = UserLinkedIn.objects.get_or_create(user=usernya)
    keahlian = KeahlianUser.objects.filter(user_linkedin=userLinkedIn).all()
    data = serializers.serialize("json", keahlian)
    return HttpResponse(data, content_type='application/json')

@csrf_exempt
def delete_user_keahlian(request):
    username = request.session['user_login']
    kode_identitas = request.session['kode_identitas']
    usernya= User.objects.get(identity_number=kode_identitas, name=username)
    userLinkedIn, berhasil = UserLinkedIn.objects.get_or_create(user=usernya)
    keahlian_id = request.POST['idnya']
    try:
        keahlianDihapus = KeahlianUser.objects.get(user_linkedin=userLinkedIn, pk=keahlian_id).delete()
        keahlian = KeahlianUser.objects.filter(user_linkedin=userLinkedIn).all()
        data = serializers.serialize("json", keahlian)
        return HttpResponse(data, content_type='application/json')
    except ObjectDoesNotExist:
        data = {
            'gagal_delete' : True,
            'idnya': keahlian_id  
        }
        return JsonResponse(data)
