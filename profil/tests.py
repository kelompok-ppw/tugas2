from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import UserLinkedIn, KeahlianUser
from .views import index
import json

# Create your tests here.
class profilUnitTest(TestCase):
	def test_profil_is_exist(self):
		response = Client().get('/profil/')
		self.assertEqual(response.status_code,200)

	def test_profil_index_func(self):
		found = resolve('/profil/')
		self.assertEqual(found.func, index)

	def test_profil_is_exist_login(self):
		session = self.client.session
		session['user_login'] = 'zaki.raihan'
		session.save()
		response = self.client.get('/profil/')
		self.assertEqual(response.status_code,302)

	def test_profil_edit_is_exist_login(self):
		session = self.client.session
		session['user_login'] = 'zaki.raihan'
		session['kode_identitas'] = '1606878505'
		session.save()
		response = self.client.get('/status/')
		response = self.client.get('/profil/profile-edit/')
		

	def test_update_profile(self):
		session = self.client.session
		session['user_login'] = 'zaki.raihan'
		session['kode_identitas'] = '1606878505'
		session.save()
		response = self.client.get('/status/')
		response2 = self.client.post('/profil/update-profile/', {'user_name' : 'Zaki Raihan', 'user_email' : '1606878505', 'user_profile' : 'Bandar Lampung',  'user_image' : '35141', 'user_headline' : 'Bengkulu'})
		response = self.client.get('/profil/profile-edit/')
		self.assertEqual(response2.status_code,200)

	def test_update_profile_no_pic(self):
		session = self.client.session
		session['user_login'] = 'zaki.raihan'
		session['kode_identitas'] = '1606878505'
		session.save()
		response = self.client.get('/status/')
		response2 = self.client.post('/profil/update-profile/', {'user_name' : 'Zaki Raihan', 'user_email' : '1606878505', 'user_profile' : 'Bandar Lampung',  'user_image' : {}, 'user_headline' : 'Bengkulu'})
		response = self.client.get('/profil/profile-edit/')
		self.assertEqual(response2.status_code,200)

	def test_update_keahlian(self):
		session = self.client.session
		session['user_login'] = 'zaki.raihan'
		session['kode_identitas'] = '1606878505'
		session.save()
		response = self.client.get('/status/')
		response = self.client.get('/profil/profile-edit/')
		response3 = self.client.post('/profil/update-keahlian/', {'user_tampilkan_nilai' : True, 'list_dari_app' : json.dumps({"0" : {"index_bahasa":"0","bahasa":"Java","index_keahlian":"0","tingkatan":"Beginner","warna_keahlian":"#42f456"}})})

	def test_get_keahlian_json(self):
		session = self.client.session
		session['user_login'] = 'zaki.raihan'
		session['kode_identitas'] = '1606878505'
		session.save()
		response = self.client.get('/status/')
		response = self.client.get('/profil/profile-edit/')
		response3 = self.client.get('/profil/get-keahlian/')

	def test_delete_keahlian(self):
		session = self.client.session
		session['user_login'] = 'zaki.raihan'
		session['kode_identitas'] = '1606878505'
		session.save()
		response = self.client
		response.get('/status/')
		response.get('/profil/profile-edit/')
		response.post('/profil/update-keahlian/', {'user_tampilkan_nilai' : True, 'list_dari_app' : json.dumps({"0" : {"index_bahasa":"0","bahasa":"Java","index_keahlian":"0","tingkatan":"Beginner","warna_keahlian":"#42f456"}})})
		response.post('/profil/delete-keahlian/', {'idnya' : 1})
		self.assertEqual(str(response), str(response))

	def test_delete_keahlian_exception(self):
		session = self.client.session
		session['user_login'] = 'zaki.raihan'
		session['kode_identitas'] = '1606878505'
		session.save()
		response = self.client
		response.get('/status/')
		response.get('/profil/profile-edit/')
		response.post('/profil/update-keahlian/', {'user_tampilkan_nilai' : True, 'list_dari_app' : json.dumps({"0" : {"index_bahasa":"0","bahasa":"Java","index_keahlian":"0","tingkatan":"Beginner","warna_keahlian":"#42f456"}})})
		response.post('/profil/delete-keahlian/', {'idnya' : 0})
		self.assertEqual(str(response), str(response))





