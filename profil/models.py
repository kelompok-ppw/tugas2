from django.db import models
from login_status.models import User

# Create your models here.
class UserLinkedIn(models.Model):
	user = models.ForeignKey(User)
	user_name = models.CharField(max_length=200, null=True)
	user_email = models.CharField(max_length=200, null=True)
	user_profile = models.URLField(max_length=200, null=True)
	user_image = models.URLField(max_length=400, null=True)
	user_tampilkan_nilai = models.BooleanField(default=False)
	user_headline = models.CharField(max_length=1000, null=True)

class KeahlianUser(models.Model):
	user_linkedin = models.ForeignKey(UserLinkedIn)
	user_index_bahasa = models.CharField(max_length=20, null=True)
	user_bahasa = models.CharField(max_length=100, null=True)
	user_index_keahlian = models.CharField(max_length=20, null=True)
	user_tingkatan = models.CharField(max_length=100, null=True)
	user_warna_keahlian = models.CharField(max_length=100, null=True)
