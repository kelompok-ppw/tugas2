from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^profile-edit/$', profile_edit, name='profile-edit'),
    url(r'^update-profile/$', update_user_profile, name='update-profile'),
    url(r'^update-keahlian/$', update_user_keahlian, name='update-keahlian'),
    url(r'^get-keahlian/$', get_keahlian_json, name='get-keahlian'),
    url(r'^delete-keahlian/$', delete_user_keahlian, name='delete-keahlian'),
]