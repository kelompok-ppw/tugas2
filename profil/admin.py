from django.contrib import admin

# Register your models here.
from .models import UserLinkedIn, KeahlianUser
from login_status.models import User, Status

admin.site.register(UserLinkedIn)
admin.site.register(KeahlianUser)
admin.site.register(User)
admin.site.register(Status)