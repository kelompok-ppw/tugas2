from django.apps import AppConfig


class LoginStatusConfig(AppConfig):
    name = 'login_status'
