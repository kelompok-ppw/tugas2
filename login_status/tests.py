import environ
import requests
from django.test import TestCase, Client
from django.urls import reverse
from django.urls import resolve
from .csui_helper import get_access_token, verify_user, get_client_id , get_data_user

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')
API_VERIFY_USER = "https://akun.cs.ui.ac.id/oauth/token/verify/"
API_MAHASISWA = "https://api-dev.cs.ui.ac.id/siakngcs/mahasiswa/"

class LoginStatusUnitTest(TestCase):
    
    #not login
    def test_company_profile_url_is_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_login_url_is_exist(self):
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 200)
        
    def test_status_url_is_exist(self):
        response = self.client.get('/status/')
        self.assertEqual(response.status_code, 200)
        
    def test_post_status_url_is_exist(self):
        response = self.client.get('/post_status/')
        self.assertEqual(response.status_code, 200)

    #login
    def test_company_profile_url_is_exist_login(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post("/auth_login/", {'username':self.username, 'password':self.password})
        response = self.client.get('/')
        self.assertEqual(response.status_code, 302)
    
    def test_login_url_is_exist_login(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post("/auth_login/", {'username':self.username, 'password':self.password})
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 302)
        
    def test_status_url_is_exist_login(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post("/auth_login/", {'username':self.username, 'password':self.password})
        response = self.client.get('/status/')
        self.assertEqual(response.status_code, 200)
        
    def test_post_status_url_is_exist_login(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post("/auth_login/", {'username':self.username, 'password':self.password})
        response = self.client.post('/post_status/',  {'status': 'stfu'})
        self.assertEqual(response.status_code, 302)
        
	#################################################################################
	# Test custom_auth
    def test_login_auth(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post("/auth_login/", {'username':self.username, 'password':self.password})
        self.assertEqual(response_post.status_code, 302)

    def test_fail_login(self):
        response_post = self.client.post("/auth_login/", {'username':'tes', 'password':'tes'})
        response = self.client.get('/login/')
        html_response = response.content.decode('utf8')
        self.assertIn('Username atau password salah',html_response)

    def test_logout_auth(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post(reverse('login-status:login'))
        response = self.client.post(reverse('login-status:auth_logout'))
        response = self.client.get('/')
        html_response = response.content.decode('utf8')
        self.assertIn('Anda berhasil logout. Semua session Anda sudah dihapus',html_response)
    
    def test_username_and_pass_wrong(self):
        username = "bersenang-senanglah"
        password = "karna hari ini kan kita rindukan"
        with self.assertRaises(Exception) as context:
            get_access_token(username, password)
        self.assertIn(username, str(context.exception))

    def test_verify_function(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        access_token = get_access_token(self.username,self.password)
        parameters = {"access_token": access_token, "client_id": get_client_id()}
        response = requests.get(API_VERIFY_USER, params=parameters)
        result = verify_user(access_token)
        self.assertEqual(result,response.json())

    def test_get_data_user_function(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        self.npm = "1606895184"
        access_token = get_access_token(self.username,self.password)
        parameters = {"access_token": access_token, "client_id": get_client_id()}
        response = requests.get(API_MAHASISWA+self.npm, params=parameters)
        result = get_data_user(access_token,self.npm)
        self.assertEqual(result,response.json())