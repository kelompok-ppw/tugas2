from django.db import models

# Create your models here.
class User(models.Model):
    identity_number = models.CharField(max_length=20)
    name = models.CharField(max_length=200)

class Status(models.Model):
    user = models.ForeignKey(User)
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
