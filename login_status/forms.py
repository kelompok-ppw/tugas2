from django import forms

class Status_Form(forms.Form):
    status = forms.CharField(required=True)
