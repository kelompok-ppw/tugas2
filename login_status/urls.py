from django.conf.urls import url
from .views import status, post_status, login, company_page
from .custom_auth import auth_login, auth_logout

urlpatterns = [
    url(r'^$', company_page, name='company-page'),
    url(r'^login/$', login, name='login'),
    
    url(r'^status/$', status, name='status'),
    url(r'^post_status', post_status, name='post-status'),

    # custom auth
    url(r'^auth_login/$', auth_login, name='auth_login'),
    url(r'^auth_logout/$', auth_logout, name='auth_logout'),

]
    