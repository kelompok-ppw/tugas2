from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import User, Status
from .forms import Status_Form
from riwayat.api_riwayat import get_riwayat
from profil.models import UserLinkedIn, KeahlianUser
from riwayat.views import hidden_or_not
from cari_mahasiswa.views import mahasiswa_list, mahasiswa_listKeahlian

# Create your views here.
response = {'author' : 'Aldi Hilman Ramadhani', 'riwayat' : get_riwayat()}
def login(request):
    print ("#==> masuk index")
    if 'user_login' in request.session:
        response['is_login'] = True
        return HttpResponseRedirect(reverse('login-status:status'))
    else:
        response['is_login'] = False
        html = 'login/login.html'
        return render(request, html, response)
        
def status(request):
    if 'user_login' in request.session:
        response['is_login'] = True
        
        username = request.session['user_login']
        kode_identitas = request.session['kode_identitas']
        
        response['user'], is_login = User.objects.get_or_create(identity_number=kode_identitas, name=username)
        response['all_status'] = Status.objects.filter(user_id=response['user'].id)
        response['user_linkedin'] = UserLinkedIn.objects.filter(user_id=response['user'].id).first()
        response['keahlian_user'] = KeahlianUser.objects.filter(user_linkedin=response['user_linkedin'])
        response['status_count'] = response['all_status'].count()
        response['status_form'] = Status_Form
        response['flag'] = hidden_or_not(request)
        response['mahasiswa_list'] = mahasiswa_list(request)
        response['mahasiswa_listKeahlian'] = mahasiswa_listKeahlian(request)
        html = 'dashboard/base.html'
        return render(request, html, response)

    response['is_login'] = False
    html = 'login/login.html'
    return render(request, html, response)
    
def post_status(request):
    form = Status_Form(request.POST or None)
    if 'user_login' in request.session and request.method == 'POST' and form.is_valid():
        response['is_login'] = True
        
        username = request.session['user_login']
        kode_identitas = request.session['kode_identitas']
        status_message = request.POST['status']
        response['user'], is_login = User.objects.get_or_create(identity_number=kode_identitas, name=username)
        new_status = Status.objects.create(description=status_message, user=response['user'])
        return HttpResponseRedirect(reverse('login-status:status'))
        
    response['is_login'] = False
    html = 'login/login.html'
    return render(request, html, response)
    
def company_page(request):
    if 'user_login' in request.session:
        response['is_login'] = True
        return HttpResponseRedirect(reverse('login-status:status'))
    else:
        response['is_login'] = False
        html = 'login/company.html'
        return render(request, html, response)